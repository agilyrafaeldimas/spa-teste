import React, { useState, useEffect, useRef } from "react";

import {
  Button,
  Col,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from "@bootstrap-styled/v4";

import api from "../services/api";

const ClientsIndex = () => {
  const [modalState, setModalState] = useState({
    modal: false,
  });

  const [client, setClient] = useState({
    name: "",
    email: "",
    phone: "",
  });

  const [search, setSearch] = useState("");
  const [clients, setClients] = useState([]);

  const [photo, setPhoto] = useState(null);

  useEffect(() => {
    api.clients.search(search).then((response) => {
      setClients(response || []);
    });
  }, [search]);

  const handleSearchChange = (event) => setSearch(event.target.value);

  const handlePhotoChange = (event) => setPhoto(event.target.files[0]);

  const handleChangeClient = (event) => {
    const target = event.target;

    const value = target.value;
    const name = target.name;

    setClient({
      ...client,
      [name]: value,
    });
  };

  const handleSaveClient = async (event) => {
    const newClient = await api.clients.store({
      ...client,
      photo,
    });

    setClients([...clients, newClient]);

    handleModalClose();
  };

  const handleModalClose = () => setModalState({ modal: !modalState.modal });

  return (
    <>
      <Row className="my-3">
        <Col>
          <Form>
            <Input
              type="text"
              placeholder="Buscar cliente por id / nome / email / telefone"
              onChange={handleSearchChange}
            />
          </Form>
        </Col>
        <Col>
          <Button
            className="float-right mr-4"
            onClick={() => handleModalClose()}
          >
            Adicionar Cliente
          </Button>
        </Col>
      </Row>

      <Row className="my-3">
        <Col>
          <Table hover>
            <Thead>
              <Tr>
                <Th>ID</Th>
                <Th>Nome</Th>
                <Th>Email</Th>
                <Th>Telefone</Th>
                <Th>Ação</Th>
              </Tr>
            </Thead>
            <Tbody>
              {clients &&
                clients.map((client) => (
                  <Tr key={client.id}>
                    <Td>{client.id}</Td>
                    <Td>{client.name}</Td>
                    <Td>{client.email}</Td>
                    <Td>{client.phone}</Td>
                    <Td>
                      <Button className="mx-2">Ver</Button>
                      <Button color="danger" className="mx-2">
                        Apagar
                      </Button>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </Col>
      </Row>

      <Modal
        size="lg"
        isOpen={modalState.modal}
        toggle={() => handleModalClose()}
      >
        <ModalHeader toggle={() => handleModalClose()}>
          Adicionar cliente
        </ModalHeader>
        <ModalBody>
          <Form>
            <Row>
              <Col>
                <Container>
                  <FormGroup row>
                    <Label htmlFor="name" sm={2}>
                      Nome
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="text"
                        name="name"
                        id="name"
                        value={client.name}
                        onChange={handleChangeClient}
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Label htmlFor="email" sm={2}>
                      Email
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="email"
                        name="email"
                        id="email"
                        value={client.email}
                        onChange={handleChangeClient}
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Label htmlFor="phone" sm={2}>
                      Telefone
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="tel"
                        name="phone"
                        id="phone"
                        value={client.phone}
                        onChange={handleChangeClient}
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Label htmlFor="photo" sm={2}>
                      Foto
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="file"
                        name="photo"
                        id="photo"
                        onChange={handlePhotoChange}
                      />
                    </Col>
                  </FormGroup>
                </Container>
              </Col>
            </Row>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => handleModalClose()}>
            Cancelar
          </Button>
          <Button color="success" onClick={handleSaveClient}>
            Salvar
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ClientsIndex;
