import React, { useState } from 'react'
import { NavLink as Link } from 'react-router-dom'


import {
    Navbar,
    Container,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink
} from '@bootstrap-styled/v4';
  
  
const Header = () => {
    const [navbarState, setNavbarState] = useState({
        isOpen: false,
    });

    return (
        <Navbar color="faded" light toggleable="lg">
            <Container>
                <div className="d-flex justify-content-between">
                    <NavbarBrand>Igreja</NavbarBrand>
                    <NavbarToggler onClick={() => setNavbarState({ isOpen: !navbarState.isOpen })} />
                </div>

                <Collapse navbar isOpen={navbarState.isOpen}>
                    <Nav navbar className="mr-auto">
                        <NavItem>
                            <NavLink tag={Link} to='/clients'>Clientes</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Container>
        </Navbar>
    )
}

export default Header