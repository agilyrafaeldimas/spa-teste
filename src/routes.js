import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./components/Header";

import ClientsIndex from './pages/ClientsIndex';
import ClientsEdit from './pages/ClientsEdit';

const Routes = () => (
  <BrowserRouter>
    <Header />
    <Switch>
      <Route exact path="/" component={() => <h1>Home</h1>} />
      <Route path="/clients" component={ClientsIndex} />
      <Route path="/clients/:id" component={ClientsEdit} />
    </Switch>
  </BrowserRouter>
);

export default Routes;