import { handleRequest } from "../api";

export default {
  async search(search) {
    const response = await handleRequest.get("clients", {
      params: { search },
    });

    return response.data || [];
  },

  async store(client) {
    const formData = new FormData();

    Object.keys(client).map((key) => formData.append(key, client[key]));

    const response = await handleRequest.post("clients", formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    });

    return response.data || false;
  },
};
