import clients from "./clients";
import axios from "axios";

export const handleRequest = axios.create({
  baseURL: "http://localhost:8080/api",
});

export default {
  clients,
};
