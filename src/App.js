import React from "react";

import { Container } from "@bootstrap-styled/v4";

import Routes from "./routes";

const App = () => (
  <Container fluid>
    <Routes />
  </Container>
);

export default App;
